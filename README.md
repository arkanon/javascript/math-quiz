# math-quiz

Math Quiz simples em HTML5+JS+CSS+MathML no estilo dos exemplos abaixo.

- http://buzzfeed.com/br/ajanibazile/teste-passar-teste-adicao-sem-calculadora
- http://buzzfeed.com/br/ajanibazile/teste-gabaritar-multiplicacoes

## Dashboard do aplicativo

- https://developers.facebook.com/apps/177456177041431

## Depurador de compartilhamento

- http://developers.facebook.com/tools/debug/?q=tiny.cc/3u97pz

## TODO

- `ok` completar o endereço ao passar o mouse sobre os botões de compartilhamento e hospedagem
- `--` fundo escuro na cor do acerto/erro no botão da página ativa
- `--` contagem dos acertos
- `--` apresentar número de compartilhamentos no botão personalizado
- `--` mensagem com o assunto da questão em cada página
- `--` salvar estatísticas anômnimas em planilha do google docs
- `--` mensagem customizada com a quantidade de acertos no compartilhamento
- `--` configuração incompleta do aplicativo em https://www.facebook.com/games/?app_id=177456177041431

