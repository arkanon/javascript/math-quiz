/*

  quiz.js

  Arkanon <arkanon@lsd.org.br>
  2020/06/09 (Tue) 14:22:50 -03

*/



    window.onload = function()
    {

      loadJSON("quiz", "quiz.hjson")

      meta = {}
      mtags = document.querySelectorAll("meta[property]")
      for (i=0; i<mtags.length; i++)
      {
        prop = mtags[i].attributes.property.value
        cont = mtags[i].attributes.content.value
        meta[prop] = cont
      }

      if ($("fb1")) $("fb1").attributes.src.value  = $("fb1").attributes.src.value  .replace( "APP_ID" , meta[ "fb:app_id" ] )
      if ($("fb2")) $("fb2").dataset.href          = $("fb2").dataset.href          .replace( "URL"    , meta[ "og:url"    ] )
      if ($("fb3")) $("fb3").attributes.href.value = $("fb3").attributes.href.value .replace( "URL"    , meta[ "og:url"    ] )



      n = 5
      N = quiz.length

      quote = encodeURIComponent('Acertei ' + n + ' de ' + N + '. Quer tentar também?')
      url   = 'https://www.facebook.com/dialog/share?app_id=' + meta["fb:app_id"] + '&href=' + encodeURIComponent(meta["og:url"])

      nww = 555                                    //  largura interna do documento desejada para a janela popup
      nwh = 593                                    //  altura  interna do documento desejada para a janela popup
      nwe = 24+59+2                                //  altura extra avaliada pela [barra de tarefas] + [barra de título e de endereço] + [borda] na nova janela
      wsw = window.screen.width                    //  largura total da tela
      wsh = window.screen.height                   //  altura  total da tela
      saw = screen.availWidth                      //  largura aproveitável da tela (sem painéis/barra de tarefas)
      sah = screen.availHeight                     //  altura  aproveitável da tela (sem painéis/barra de tarefas)
      wsx = window.screenX                         //  == screenLeft  posição y do lado de cima  externo da janela
      wsy = window.screenY                         //  == screenTop   posição x do lado esquerdo externo da janela
      wiw = window.innerWidth                      //  largura interna visivel do document com barra de rolagem vertical
      wih = window.innerHeight                     //  altura  interna visivel do document com barra de rolagem horizontal
      wow = window.outerWidth                      //  largura externa da janela sem decorações (bordas e barra de título)
      woh = window.outerHeight                     //  altura  externa da janela sem decorações (bordas e barra de título)
      dsw = document.documentElement.scrollWidth   //  largura total do documento incluindo a parte escondida pelo overflow
      dsh = document.documentElement.scrollHeight  //  altura  total do documento incluindo a parte escondida pelo overflow
      sbw = getScrollBarWidth()                    //  largura da barra de scroll
      ecw = document.documentElement.clientWidth   //  largura interna visivel do document com barra de rolagem vertical
      dcw = ecw - sbw*(ecw!=dsw?1:0)               //  largura interna visivel do document sem barra de rolagem vertical
      dch = document.documentElement.clientHeight  //  altura  interna visivel do document sem barra de rolagem horizontal
      nwx = wsx + ( dcw - nww )/2                  //  posição x da nova janela centralizada sobre a janela pai
      nwy = wsy + ( dch - nwh )/2 + nwe            //  posição y da nova janela centralizada sobre a janela pai

      console.log( 'nww = ' + nww )
      console.log( 'nwh = ' + nwh )
      console.log( 'nwe = ' + nwe )
      console.log( 'wsw = ' + wsw )
      console.log( 'wsh = ' + wsh )
      console.log( 'saw = ' + saw )
      console.log( 'sah = ' + sah )
      console.log( 'wsx = ' + wsx )
      console.log( 'wsy = ' + wsy )
      console.log( 'wiw = ' + wiw )
      console.log( 'wih = ' + wih )
      console.log( 'wow = ' + wow )
      console.log( 'woh = ' + woh )
      console.log( 'dsw = ' + dsw )
      console.log( 'dsh = ' + dsh )
      console.log( 'sbw = ' + sbw )
      console.log( 'ecw = ' + ecw )
      console.log( 'dcw = ' + dcw )
      console.log( 'dch = ' + dch )
      console.log( 'nwx = ' + nwx )
      console.log( 'nwy = ' + nwy )

      wname      = "share"
      share      = $("share")
      share.href = url
      share.addEventListener
      (
        'click',
        function(e)
        {
          e.preventDefault()
          var win  = window.open( url+'&quote='+quote , wname , [ 'width='+nww , 'height='+nwh , 'left='+nwx , 'top='+nwy ].join() )
          win.opener = null // desativa a reutilização da janela aberta para o caso dela ficar perdida embaixo de outras
        }
      )



      ctx = document.createElement("canvas").getContext("2d")

      shuffle(quiz)

      for ( i=0; i<quiz.length; i++ )
      {

        /*
           ok <div id="q1" class="page">
           ok   <div class="q"><math></math></div>
           ok   <div>
           ok     <div class="a"><math></math></div>
           ok     <div class="a"><math></math></div>
                </div>
           ok   <div>
           ok     <div class="a"><math></math></div>
           ok     <div class="a x"><math></math></div>
                </div>
              </div>
        */

        q  = quiz[i]["q"]
        a  = quiz[i]["a"]

        qm = document.createElement("math")
        qm.innerHTML = q
        qd = document.createElement("div")
        qd.className = "q"
        qd.appendChild(qm)

        adl = []
        for ( j=0; j<a.length; j++ )
        {
          am = document.createElement("math")
          am.innerHTML = a[j]
          ad = document.createElement("div")
          ad.className = "a a-hover"
          if (j==0) ad.setAttribute("data-a", "1")
          ad.setAttribute("onclick", "check(this)")
          ad.appendChild(am)
          adl.push(ad)
        }
        shuffle(adl)

        ad1 = document.createElement("div")
        ad1.appendChild(adl[0])
        ad1.appendChild(adl[1])

        ad2 = document.createElement("div")
        ad2.appendChild(adl[2])
        ad2.appendChild(adl[3])

        page = document.createElement("div")
        page.className = "page"
        page.id = "q"+(i+1)
        page.appendChild(qd)
        page.appendChild(ad1)
        page.appendChild(ad2)

        $("pages").appendChild(page)



        but = document.createElement("button")
        but.innerText = (i+1)
        but.className = "tablinks"
        but.id        = "b"+(i+1)
        but.setAttribute("onclick", "goto(event,'q"+(i+1)+"')")
        $("chooser").appendChild(but)

      }

      MathJax.typeset()

      $("warning").className = "fadeOut"
      $("warning").addEventListener
      (
        "animationend",
        function()
        {
          this.remove()
          $("chooser").style.visibility = "visible"
          if ($("b1")) $("b1").click()
        }
      )

    }



    function $(id) { return document.getElementById(id) }



    // Getting scroll bar width using JavaScript <http://stackoverflow.com/a/13382873>
    function getScrollBarWidth()
    {
      // creating invisible container
      const outer = document.createElement('div')
      outer.style.visibility      = 'hidden'
      outer.style.overflow        = 'scroll'    // forcing scrollbar to appear
      outer.style.msOverflowStyle = 'scrollbar' // needed for WinJS apps
      document.body.appendChild(outer)
      // creating inner element and placing it in the container
      const inner = document.createElement('div')
      outer.appendChild(inner)
      // calculating difference between container's full width and the child width
      const scrollbarWidth = (outer.offsetWidth - inner.offsetWidth)
      // removing temporary elements from the DOM
      outer.parentNode.removeChild(outer)
      return scrollbarWidth
    }



    // <http://w3schools.com/howto/howto_js_tabs.asp>
    function goto(e, page)
    {
      var i, page, tablinks
      pages = document.getElementsByClassName("page")
      for ( i=0; i<pages.length; i++ ) pages[i].style.display = "none"
      tablinks = document.getElementsByClassName("tablinks")
      for ( i=0; i<tablinks.length; i++ )
      {
        tablinks[i].className = tablinks[i].className.replace(" active", "")
      }
      bg = e.currentTarget.style.backgroundColor
      use = ""
console.log(e.currentTarget)
console.log("bg: ["+bg+"]")
      if (bg!="")
      {
        ctx.strokeStyle = bg // How to get the background color cod.tab button.e of an element in hex <http://stackoverflow.com/a/5999319>
        hexColor = ctx.strokeStyle
console.log("bg: "+bg+"\nhexColor: "+hexColor)
        if ( hexColor == "#ff9999" ) use = " awrong"
        if ( hexColor == "#79d279" ) use = " aright"
      }
      $(page).style.display = "block"
      e.currentTarget.className += " active" + use
    }



    /*
     * Randomly shuffle an array
     * <http://gomakethings.com/how-to-shuffle-an-array-with-vanilla-js>
     * <http://stackoverflow.com/a/2450976/1293256>
     * @param  {Array} array The array to shuffle
     * @return {String}      The first item in the shuffled array
     */
    function shuffle(array)
    {
      var currentIndex = array.length
      var temporaryValue, randomIndex
      // while there remain elements to shuffle...
      while (0 !== currentIndex)
      {
        // pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex)
        currentIndex -= 1
        // and swap it with the current element
        temporaryValue = array[currentIndex]
        array[currentIndex] = array[randomIndex]
        array[randomIndex] = temporaryValue
      }
      return array
    }



    function check(el)
    {
      id  = el.parentElement.parentElement.id
      but = id.replace("q","b")
      a   = document.querySelectorAll( "div#" + id + " > div > div.a" )
      for ( i=0; i<a.length; i++ )
      {
        if (el.dataset.a)
        {
          a[i].className = a[i] == el ? "a a-right" : "a"
          $(but).className += " right"
        }
        else
        {
          a[i].className = a[i] == el ? "a a-wrong" : "a"
          $(but).className += " wrong"
        }
        if (a[i].dataset.a) a[i].className = "a a-right"
        a[i].setAttribute("onclick", "")
      }
    }



    function loadJSON(varname, url)
    {
      var xobj = new XMLHttpRequest()
      xobj.overrideMimeType("application/json")
      xobj.open("GET", url, false) //  false := REQUISIÇÃO SINCRONA
      xobj.onreadystatechange = function()
      {
        if (xobj.readyState == 4 && xobj.status == "200") window[varname] = JSON5.parse(xobj.responseText) // <http://json5.org>
      }
      xobj.send(null)
    }



/* EOF */
